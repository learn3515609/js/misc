// const numbers = {
//   length: 5,
//   [Symbol.iterator]: function*  () {
//       for(let i = 0; i < this.length; i++) {
//         yield i;
//       }
//     }
// }

const numbers = {
    length: 5,
    [Symbol.iterator]: function  () {
      let i = -1;
        return {
          next: () => {
            i++;
            return {
              value: i,
              done: i === this.length
            }
          }
        }
      }
  }

for(let i of numbers) {
  console.log('first type: ', i);
}

const iterator = numbers[Symbol.iterator]();
let i;
while(i = iterator.next()) {
  if(i.done) break;
  console.log('second type: ', i.value);
}